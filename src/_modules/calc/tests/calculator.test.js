'use strict';

// default import
import calculator from '../calculator';

describe('test imported functions', function() {

  it('Should use default imports', () => {
    
    expect(calculator).toBeDefined();
  });
  
  it('Should use default imports', () => {
    //var calculatorDef = calculator;
    var mycalc = calculator();
    expect(mycalc.init()).toBe(true);
    calculator.reset();
    expect(calculator.getResult()).toBe(0);

    calculator.add(10);
    calculator.add(20);
    expect(calculator.getResult()).toBe(30);
    expect(calculator.getVersion()).toBe(1);
  });

});