
var result = 0;


/**
 * Refactoring procedure
 * 1. Identify the exported object (default module.exports 
 * or explicitly allocated function/object )
 * 2. Export default that object
 * Probably named exports should not be used in 
 * the refactoring. The reason is that CommonJS
 * always exports as single default object with
 * a series of properties assigned to it.
 */

exports = calculator;

function calculator(){
  var app = {
    init: function (){
      return true;
    }
  };
  return app;
}

function add(a){
  result += a;
}

function getResult(){
  return result;
}

function reset(){
  result = 0;
}

calculator.getVersion = function(){
  return 1;
};

exports.add = add;
exports.getResult = getResult;
exports.reset = reset;

export default calculator;