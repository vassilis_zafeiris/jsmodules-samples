import * as counter1 from '../counter';
import * as counter2 from '../counter';
import * as scalarCounter1 from '../counter';
import * as scalarCounter2 from '../counter';

describe('counter', () => {

  it('should not copy object structure on require', () => {

    counter1.increaseVal1();
    expect(counter1.getCounterVal1()).toBe(1);

    expect(counter2.getCounterVal1()).toBe(1);
    counter2.increaseVal1();

    expect(counter1.getCounterVal1()).toBe(2);
    expect(counter2.getCounterVal1()).toBe(2);

    // modify local copy
    expect(counter2.getCounterVal1()).toBe(2);
    expect(counter1.getCounterVal2()).toBe(0);
    counter1.counter.val1 = 10;
    counter2.counter.val2 = 20;
    expect(counter2.getCounterVal1()).toBe(10);
    expect(counter1.getCounterVal2()).toBe(20);

    expect(window.executed).toBe(1);

  });


  it('should copy scalar values on require', () => {

    scalarCounter1.increaseScalar();
    expect(scalarCounter1.getScalarCounter()).toBe(1);
    expect(scalarCounter2.getScalarCounter()).toBe(1);

    // seems that es6 makes a shallow copy on each import
    //expect(scalarCounter1 == scalarCounter2).toBe(true);
    
    /*
    scalarCounter1.scalarCounter++;
    // the exported scalar is different variable from the module variable
    // This is expected since the exported object does not hold a reference to the module variable, but a copy of it during export time
    expect(scalarCounter1.getScalarCounter()).toBe(1);
    expect(scalarCounter2.getScalarCounter()).toBe(1);
    
    expect(scalarCounter1.scalarCounter).toBe(1);
    console.log(scalarCounter1);
    expect(scalarCounter2.scalarCounter).toBe(1);

    
    scalarCounter2.scalarCounter = 10;
    // the exported variable is accessed by all references to the required object
    expect(scalarCounter1.getScalarCounter()).toBe(1);
    expect(scalarCounter2.getScalarCounter()).toBe(1);
    expect(scalarCounter1.scalarCounter).toBe(10);
    expect(scalarCounter2.scalarCounter).toBe(10);
    */
  });

});
