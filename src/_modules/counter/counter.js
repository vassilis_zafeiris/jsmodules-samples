
if (window.executed != undefined){
  window.executed++;
} else {
  window.executed = 1;
}

var counter = {
  val1: 0,
  val2: 0
};

var scalarCounter = 0;

function incrScalar(){
  scalarCounter++;
}

function decrScalar(){
  scalarCounter++;
}

function getScalarCounter(){
  return scalarCounter;
}

function incCounterVal1(){
  counter.val1++;
}

function decrCounterVal1(){
  counter.val1--;
}

function getCounterVal1(){
  return counter.val1;
}

function incCounterVal2(){
  counter.val2++;
}

function decrCounterVal2(){
  counter.val2--;
}

function getCounterVal2(){
  return counter.val2;
}

var exportedObj = {
  counter: counter,
  increaseVal1: incCounterVal1,
  decreaseVal1: decrCounterVal1,
  getCounterVal1: getCounterVal1,
  getCounterVal2: getCounterVal2,
  scalarCounter: scalarCounter,
  increaseScalar: incrScalar,
  decreaseScalar: decrScalar,
  getScalarCounter: getScalarCounter
};

export default exportedObj;