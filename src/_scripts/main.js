// Main javascript entry point
// Should handle bootstrapping/starting application

'use strict';

console.log('main module evaluated');

import "../_modules/mods/modA";
import $ from 'jquery';
import Link from '../_modules/link/link';
import {result} from '../_modules/calc/calculator';
import {add, getResult} from '../_modules/calc/calculator';



$(() => {
  new Link(); // Activate Link modules logic
  console.log('Welcome to Yeogurt!');

  console.log('Result:' + result);

  add(10);

  console.log('Result:' + getResult());
  console.log('Result:' + result);

  // result is read-only error
  //result += 20;

});
